<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/hello', 'StorageController@hello');
Route::post('/saving', 'StorageController@saving');
Route::post('/save_image', 'StorageController@save_image');
Route::post('/save_to_my_designs', 'StorageController@save_to_my_designs');
Route::get('/my_designs', 'StorageController@my_designs');
Route::get('/delete_design', 'StorageController@delete_design');

Route::get('/', function () {
    return view('welcome');
});
