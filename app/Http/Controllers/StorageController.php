<?php

namespace App\Http\Controllers;
use App\FileUpload;
use Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Template;
use Intervention\Image\ImageManagerStatic as Image;

class StorageController extends Controller
{
	/*
	public function store(Request $request)
	    {
	       if($request->get('image'))
	       {
	          $image = $request->get('image');
	          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
	          \Image::make($request->get('image'))->save(public_path('images/').$name);
	        }

	       $image= new FileUpload();
	       $image->image_name = $name;
	       $image->save();

	       return response()->json(['success' => 'You have successfully uploaded an image'], 200);
	     }
	*/
		 
		 public function hello(){
			Log::info("entro hello GET");
			$id = input::get('ID');
			Log::info("ID: $id");
		
			return response()->json(['success' => 'You have successfully hello touch'], 200);
		 }
		 
		 public function saving(){
		 	
 			Log::info("entro saving");
 			$user_id = input::get('user_id');
 			$odata = input::get('odata');
			$fileName = uniqid() . '.json' ;
			
			$template = new Template();
			$template->json_file = $fileName;
			$template->user_id = $user_id;
			$template->save();
			
			Storage::disk('local')->put("public/$user_id/$fileName",json_encode($odata));	
 			return response()->json(['success' => 'File Saved']);
		 }
		 
		 public function save_image(){
		 	Log::info("entro save_image");
			$image_data = input::get('image_data');
			$user_id = input::get('user_id');
			
			Log::info("user_id:");
			Log::info($user_id);
			Log::info("image_data");
			Log::info($image_data);
			
		    $image_parts = explode(";base64,", $image_data);
		    $image_type_aux = explode("image/", $image_parts[0]);
		    $image_type = $image_type_aux[1];
		    $image_base64 = base64_decode($image_parts[1]);
		    $fileName = uniqid() . '.png';
			
			Storage::disk('local')->put("public/$user_id/$fileName",$image_base64);
			
			return response()->json(['success' => 'File Saved']);
		 }
		 
		 public function save_to_my_designs(){
			 
		 	Log::info("entro save_to_my_designs");
			$thumbnail_file = input::get('thumbnail_file');
			$user_id = input::get('user_id');
			$json_file = input::get('json_file');
			
			//STORAGE FULL IMAGE
			/*
		    $image_parts = explode(";base64,", $thumbnail_file);
		    $image_type_aux = explode("image/", $image_parts[0]);
		    $image_type = $image_type_aux[1];
		    $image_base64 = base64_decode($image_parts[1]);
		    $thumbnailFileName = uniqid() . '.png';
			$icon_path = "storage/$user_id/icon$thumbnailFileName";
			Storage::disk('local')->put("public/$user_id/$thumbnailFileName",$image_base64);
			Log::info("thumbnailFileName: $thumbnailFileName");
			*/
			
			//IF FOLDE DOESN'T EXIST CREATE ONE
			$dir = public_path("storage/templates/$user_id");
			if( ! \File::isDirectory($dir) ) {

			        // Params:
			        // $dir = name of new directory
			        //
			        // 493 = $mode of mkdir() function that is used file File::makeDirectory (493 is used by default in \File::makeDirectory
			        //
			        // true -> this says, that folders are created recursively here! Example:
			        // you want to create a directory in company_img/username and the folder company_img does not
			        // exist. This function will fail without setting the 3rd param to true
			        // http://php.net/mkdir  is used by this function

			        \File::makeDirectory($dir, 493, true);
			}
			
			
		    $image_parts = explode(";base64,", $thumbnail_file);
		    $image_type_aux = explode("image/", $image_parts[0]);
			$thumbnailFileName = 'icon' . uniqid() . '.png';
			$icon_path = "storage/templates/$user_id/$thumbnailFileName";
			Image::make($image_parts[1])->resize(400, null, function ($constraint) {
			    $constraint->aspectRatio();
			    $constraint->upsize();
			})->save($icon_path);
			
			//STORAGE JSON
			$JSONFileName = uniqid() . '.json' ;
			Storage::disk('local')->put("public/templates/$user_id/$JSONFileName",json_encode($json_file));
			Log::info("JSONFileName: $JSONFileName");
			
			$template = new Template();
			$template->json_file = $JSONFileName;
			$template->thumbnail_file = $thumbnailFileName;
			$template->user_id = $user_id;
			$template->title = input::get('title');
			
			$template->save();
			
			return response()->json(['success' => 'File Saved']);
		 }
		 
		 public function delete_design(){
		 	$design = Template::findOrFail(input::get('design_id'));
			$design->delete();
			return response()->json(['success' => 'File Deleted']);
		 }
		 
		 public function my_designs(){
			$user_id = input::get('user_id');
			Log::info("entro en my_designs");
			Log::info("user_id");
			Log::info($user_id);
			
			$base_url = env('APP_URL')."/storage";
			
			$designs = Template::where("user_id",$user_id)->orderBy('id', 'desc')->get();
			Log::info("designs arrray:");
			Log::info($designs);
			//return response()->json(["designs" => $designs, "base_url" => $base_url]);
			return response()->json($designs);
			
		 }
		 
}