<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class ResetUserTemplates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	//php artisan reset_user_templates --user_id=12
    protected $signature = 'reset_user_templates {--user_id=user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset user templates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$user_id = $this->option('user_id');
		DB::table('templates')->where('user_id', $user_id)->delete();

    }
}
